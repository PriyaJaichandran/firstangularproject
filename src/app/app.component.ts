import { Component} from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { GlobalConstants } from './globalconstants';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'invokeapi';
  product;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get<any>(GlobalConstants.productapi).subscribe(data => {
      console.log(data);
      this.product = data;
    })
  }
}
